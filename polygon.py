# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 15:07:20 2019

@author: Student
"""

from wall import Wall
from particle import Particle
from vector2 import vector2
import pygame

class Polygon(Particle):
    def __init__(self, points, color=(0,0,0)):
        super().__init__(0, vector2(0,0))
        self.color = color
        self.points = []
        self.normals = []
        self.walls = []
        self.sides = len(points)
        for p in points:
            self.points.append(vector2(p))
        for i in range(len(self.points)):
            self.normals.append(-1*(self.points[i] - self.points[i-1]).perp().hat())
            wall = Wall(self.points[i], self.normals[i])
            self.walls.append(wall)
        for i in range(len(self.normals)):
            for p in self.points:
                pos = 0
                neg = 0
                d = (p-self.points[i]) * self.normals[i]
                if d > 0:
                    pos += 1
                elif d < 0:
                    neg += 1
                if pos > 0:
                    if neg == 0:
                        self.normals[i] *= -1
                    else:
                        raise(ValueError,"non con vex polygon")
        
        
    def draw(self, screen):
        pygame.draw.polygon(screen, self.color, self.points)
        for i in range(len(self.normals)):
            pygame.draw.line(screen, (0,0,0), self.points[i], (self.points[i]+50*self.normals[i]).pygame())
            
        