# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 13:26:37 2019

@author: Student
"""

from wall import Wall
from particle import Particle
from vector2 import vector2
import pygame

class Hill(Particle):
  
    def __init__(self, force, normals, points, color=(0,0,0)):
        super().__init__(0, points[0])
        self.color = color
        self.force = force
        self.points = []
        self.normals = normals
        for p in points:
            self.points.append(vector2(p))
        
        
    def draw(self, screen):
        pygame.draw.polygon(screen, self.color, self.points, 5)
            
        