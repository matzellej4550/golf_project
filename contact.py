# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 11:14:47 2019

@author: sinkovitsd
"""
from vector2 import vector2
from circle import Circle
from math import sqrt
from wall import Wall
from polygon import Polygon

class Contact:
    def __init__(self, obj1, obj2, restitution, normal, penetration):
        self.obj1 = obj1
        self.obj2 = obj2
        self.restitution = restitution
        self.normal = normal
        self.penetration = penetration
        
    def resolve(self):
        self.resolve_velocity()
        self.resolve_penetration()
        
    def resolve_penetration(self):
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            nudge = self.penetration/total_invmass
            self.obj1.pos += (self.obj1.invmass*nudge)*self.normal
            self.obj2.pos -= (self.obj2.invmass*nudge)*self.normal

    def resolve_velocity(self):
        sep_vel = (self.obj1.vel - self.obj2.vel)*self.normal
        if sep_vel < 0:
            target_sep_vel = -self.restitution*sep_vel
            delta_vel = target_sep_vel - sep_vel
            total_invmass = self.obj1.invmass + self.obj2.invmass
            if total_invmass > 0:
                impulse = delta_vel/total_invmass
                self.obj1.vel += (self.obj1.invmass*impulse)*self.normal
                self.obj2.vel -= (self.obj2.invmass*impulse)*self.normal

class ContactGenerator:
    def __init__(self, objects, restitution=0):
        self.objects = objects
        self.restitution = restitution
        for o in objects:
            o.contacts.append(self)
        self.contacts = []
    
    def remove(self, obj):
        self.objects.remove(obj)
        
    def contact_all(self):
        self.contacts.clear()
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                # circle circle
                if isinstance(obj1, Circle) and isinstance(obj2, Circle):
                    self.contact_circle_circle(obj1, obj2)
                # wall circle
                elif isinstance(obj1, Wall) and isinstance(obj2, Circle):
                    self.contact_circle_wall(obj1, obj2)
               # circle wall
                elif isinstance(obj1, Circle) and isinstance(obj2, Wall):
                    1 + 1
                # wall wall
                elif isinstance(obj1, Wall) and isinstance(obj2, Wall):
                    1 + 1
                # polygon circle
                elif isinstance(obj1, Polygon) and isinstance(obj2, Circle):
                    self.contact_circle_polygon(obj1, obj2)
                # circle polygon
                elif isinstance(obj1, Circle) and isinstance(obj2, Polygon):
                    1 + 1
                # polygon wall
                elif isinstance(obj1, Polygon) and isinstance(obj2, Wall):
                    1 + 1
                # polygon polygon
                elif isinstance(obj1, Polygon) and isinstance(obj2, Polygon):
                    1 + 1
                else:
                    print("Warning! ContactGenerator not implemented between ",
                          type(obj1)," and ", type(obj2),".")
        return self.contacts

    def contact_circle_circle(self, obj1, obj2):
        r = obj1.pos - obj2.pos
        rmag2 = r.mag2()
        R = obj1.radius + obj2.radius
        if rmag2 < R*R:
            rmag = sqrt(rmag2)
            penetration = R - rmag
            normal = r/rmag
            self.contacts.append(Contact(obj1, obj2, self.restitution, normal, penetration))

    def contact_circle_wall(self, obj1, obj2):
        R = obj2.pos * obj1.normal - obj1.pos * obj1.normal
        if R > -obj2.radius:
            #print("Eat Bug")
            penetration = obj2.radius - (obj1.pos - obj2.pos) * obj1.normal
            self.contacts.append(Contact(obj1, obj2, self.restitution, obj1.normal, penetration))
        
            
    def contact_circle_polygon(self, obj1, obj2):
        # obj1 is polygon
        # obj2 is circle
        least_p = 1e99
        p = vector2(0,0)
        for i in range(len(obj1.points)):
            p = (obj1.points[i] - obj2.pos) * obj1.normals[i] + obj2.radius
            
            if p <= 0:
                # exit
                return
            if p < least_p:
                least_p = p
                least_n = obj1.normals[i]
           
        # loop over all vertices; find nearest, rv
        least_d2 = 1e99
        
        for o in obj1.points:
            d2 = (o - obj2.pos).mag2()
            if d2 < least_d2:
                least_d2 = d2
                least_r = o
        n_hat = (least_r - obj2.pos).hat()
        wall_pos = obj2.pos + obj2.radius * n_hat
        max_p = -1e99
        for r in obj1.points:
            # one of these isn't a vector2!?
            # we know that n_hat is a vector2 so...?
            # print("p is a ", p.type)
            # should be p not r but for some reason p is not working
            p = (wall_pos - r)*n_hat
            if p > max_p:
                max_p = p
        
        #r = obj2.pos + obj2.radius*n_hat
        #p = (r - least_r) * n_hat
            
        if max_p < 0:
            return
        if max_p < least_p:
            self.contacts.append(Contact(obj1, obj2, self.restitution,  n_hat, max_p))
        else: # circle side
            self.contacts.append(Contact(obj2, obj1, self.restitution, least_n, least_p))
            
        
